<?php

/**
 * Displays articles from a single category
 */
class Single_Category extends WP_Widget {
    // default sorting order of this widget
    private $default_sort;
    
    // default number of articles to display
    private $default_number;
    
    /*
     * Class constructor
     *
     * @return void
     */
    function __construct() {
        $widget_ops = array(
            'classname' => 'single_category',
            'description' => __('Displays articles from a single category', 'single-category')
        );
        
        // set default values for this widget
        $this->default_sort   = 'post_title';
        $this->default_number = 5;
        
        parent::__construct(
            'single_category',
            __( 'Single Category', 'single-category' ),
            $widget_ops
        );
    }
    
    /**
	 * Outputs the HTML for this widget
	 *
	 * @param array An array of standard parameters for widgets in this theme
	 * @param array An array of settings for this widget instance
	 *
	 * @return void
	 */
	function widget($args, $instance) {
	    extract($args);

		$title = apply_filters(
		    'widget_title',
		    empty($instance['title']) 
		        ? __( 'Single Category', 'single-category' )
		        : $instance['title'],
		    $instance, $this->id_base
		);
		
		$sortby = empty($instance['sortby'])
		    ? $this->default_sort : $instance['sortby'];
		
		$category = $instance['category'];
		
		$number = empty($instance['number'])
		    ? $this->default_number : $instance['number'];
		
		if ($sortby == 'menu_order') {
			$sortby = 'menu_order, post_title';
		}
		
		$posts = get_posts(array(
		    'numberposts' => $number,
		    'category'    => $category,
		    'orderby'     => $sortby
		));
		
		if (!empty($posts)) {
		    global $post;
		    
		    echo $before_widget;
		    
		    if (!empty($title)) {
		        echo $before_title . $title . $after_title;
		    } ?>
	        
            <?php foreach($posts as $post): setup_postdata($post); ?>
                <div class="single-category-post">
                    <?php if ($instance['show_thumbs']): ?>
                        <div class="single-category-post-image">
                            <?php the_post_thumbnail(); ?>
                        </div>
                    <?php endif; ?>
                    <h4 class="single-category-post-title">
                        <?php the_title(); ?>
                    </h4>
                    <div class="single-category-post-text">
                        <?php the_excerpt();; ?>
                    </div>
                    <a href="<?php the_permalink(); ?>"
                        class="single-category-post-link"
                    >
                        Learn More...
                    </a>
                    <div class="clear"></div>
                </div>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
	        
		    <?php echo $after_widget;
		}
	}
	
	/*
	 * Saves changes made to this widget's settings
	 *
	 * @param array Array of new settings for this widget instance to be saved
	 * @param array Array of current settings for this widget instance
	 *
	 * @return Array of settings to save for this widget
	 */
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		
		// saves the new widget title
		$instance['title'] = strip_tags($new_instance['title']);
		
		// saves the new widget "Sort By" preference
		if (in_array(
		    $new_instance['sortby'],
		    array('post_title', 'menu_order', 'ID')
		)) {
			$instance['sortby'] = $new_instance['sortby'];
		} else {
			$instance['sortby'] = $this->default_sort;
		}

        // saves the new widget category
		$instance['category'] = strip_tags($new_instance['category']);
		
		// saves the new widget "Number of Articles" preference
		if ($new_instance['number'] > 0) {
		    $instance['number'] = $new_instance['number'];
		} else {
		    $instance['number'] = $this->default_number;
		}
		
		// saves whether post thumbnails are shown
		if (isset($new_instance['show_thumbs'])) {
		    $instance['show_thumbs'] = true;
		} else {
		    $instance['show_thumbs'] = false;
		}
		
		error_log(print_r($new_instance, true));

		return $instance;
	}

    /**
	 * Displays the form for this widget on
	 * the Widgets page of the WP Admin area.
	 *
	 * @param array Array of settings for this widget instance
	 */
	function form($instance) {
        $instance = wp_parse_args((array) $instance, array(
            'sortby' => $this->default_sort,
            'title' => '',
            'category' => '',
            'number' => $this->default_number,
            'show_thumbs' => false
        ));
        
        // Gets the widget title
        $title = esc_attr($instance['title']);
        
        // Gets the selected category
        $category = esc_attr($instance['category']);
        
        // Gets an array of categories
        $categories = get_categories();
        
        // Gets the number of posts to display
        $number = esc_attr($instance['number']);
        
        // Gets whether the show thumbnails option is checked
        $show_thumbs = esc_attr($instance['show_thumbs']);
        
        ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>">
                    <?php _e('Title:'); ?>
                </label>
                <input class="widefat"
                    id="<?php echo $this->get_field_id('title'); ?>"
                    name="<?php echo $this->get_field_name('title'); ?>"
                    type="text"
                    value="<?php echo $title; ?>"
                >
            </p>
            
            <p>
                <label for="<?php echo $this->get_field_id('sortby'); ?>">
                    <?php _e('Sort by:'); ?>
                </label>
                <select name="<?php echo $this->get_field_name('sortby'); ?>"
                    id="<?php echo $this->get_field_id('sortby'); ?>"
                    class="widefat"
                >
                    <option value="post_title"
                        <?php selected($instance['sortby'], 'post_title'); ?>
                    >
                        <?php _e('Page title'); ?>
                    </option>
                    <option value="menu_order"
                        <?php selected($instance['sortby'], 'menu_order'); ?>
                    >
                        <?php _e('Page order'); ?>
                    </option>
                    <option value="ID"
                        <?php selected($instance['sortby'], 'ID'); ?>
                    >
                        <?php _e('Page ID'); ?>
                    </option>
                </select>
            </p>
            
            <p>
                <label for="<?php echo $this->get_field_id('category'); ?>">
                    <?php _e( 'Single Category', 'single-category' ) ?>
                </label>
                <select name="<?php echo $this->get_field_name('category'); ?>"
                    id="<?php echo $this->get_field_id('category'); ?>"
                    class="widefat"
                >
                    <?php foreach ($categories as $c): ?>
                        <option value="<?php echo $c->cat_ID; ?>"
                            <?php
                                selected($instance['category'], $c->cat_ID);
                            ?>
                        >
                            <?php echo $c->name; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </p>
            
            <p>
                <label for="<?php echo $this->get_field_id('number'); ?>">
                    <?php
                        _e( 'Number of Posts to display:', 'single-category' )
                    ?>
                </label>
                <input class="widefat"
                    id="<?php echo $this->get_field_id('number'); ?>"
                    name="<?php echo $this->get_field_name('number'); ?>"
                    type="text"
                    value="<?php echo $number; ?>"
                >
            </p>
            
            <p>
                <input id="<?php echo $this->get_field_id('show_thumbs'); ?>"
                    name="<?php echo $this->get_field_name('show_thumbs'); ?>"
                    type="checkbox"
                    <?php checked($show_thumbs, true) ?>
                >
                <label for="<?php echo $this->get_field_id('show_thumbs'); ?>">
                    <?php _e( 'Show post thumbnails?', 'single-category' ); ?>
                </label>
            </p>
        <?php
	}
}

