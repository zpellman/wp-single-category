<?php
/*
Plugin Name: Single Category Widget
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: Adds a widget that displays posts from a single category
Version: 1.0
Author: Zach Pellman
Author URI: http://www.zachpellman.com
License: GPL2

Copyright 2012 Zach Pellman <zpellman@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

require_once 'class-single-category.php';

// Initializes the Single Category widget
function initialize_single_category_widget() {
    // registers the widget with WordPress
    register_widget( 'Single_Category' );
    
    // loads the widget's language file(s)
    $plugin_dir = basename( dirname( __FILE__ ) ) . '/languages';
    load_plugin_textdomain( 'single-category', false, $plugin_dir );
}

// creates an action hook to register the Single Category widget on widgets_init
add_action('widgets_init', 'initialize_single_category_widget');
